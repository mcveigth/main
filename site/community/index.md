
# Comunidad

## Miembros

Aunque GuayaHack es una iniciativa creada por {doc}`/community/member/jdsalaro/index`, es un esfuerzo colaborativo de tod@s para tod@s, estos son sus miembros:

## Voluntarios

### Moderadores

Todos los moderadores son tutores.

1. {doc}`/community/member/jdsalaro/index`
1. {doc}`/community/member/rioschala/index`
1. {doc}`/community/member/nikoresu/index`
1. NOMBRE_DISCORD
1. NOMBRE_DISCORD

### Tutores

Todos los tutores son participantes.

1. NOMBRE_DISCORD
1. NOMBRE_DISCORD
1. NOMBRE_DISCORD
1. NOMBRE_DISCORD

### Participantes

1. {doc}`/community/member/danteboe/index`
1. {doc}`/community/member/draccobandi/index` 
1. {doc}`/community/member/shavenbaboon/index`
1. {doc}`/community/member/sagajj/index`
1. NOMBRE_DISCORD
