
```{post} 2023-07-18
:author: "@rioschala"
:tags: organización, documentación
:category: wiki
:language: Spanish
:location: Colombia
:excerpt: 1
```

# ¿Cómo documentar utilizando las herramientas de Windows?

En el proceso de documentación, es importante considerar todas las herramientas que puedan sernos útiles en los procesos de documentación de las tareas. Para ello, se propone desde la comunidad seguir lineamientos claros

## Utilizar Herramienta de Recortes
En computadores que tengan sistema operativo Windows, es importante considerar todas las herramientas. Sin embargo, para que los recursos sean visibles, deben ser recortados a la medida de la información que se requiere. Para eso, se debe utilizar la herramienta de recortes. Para acceder a ella, se debe presionar la combinación de teclas:

```{figure} como-documentar.md-data/windows-shitf-s.png
---
---
Combinación de teclas en Windows
```
Además, pueden buscar en Windows Snipping Tool:

```{figure} como-documentar.md-data/snipping-tool-shortcut.png
---
---
Búsqueda de aplicación
``` 
## ¿Qué se debe cumplir?

1. Ajustar los bordes de la imagen a la información que se quiere presentar. De esta forma, se asegura solo la información relevante para la documentación.
2. Nombrar los documentos con nombres relevantes que permitan su ubicación corta y su fácil referenciación. 
3. Incluirla dentro de las publicaciones que se requieran. Si no es necesario, por favor, no subirlos dentro del repo. 


```console
$ git status 
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean
```
